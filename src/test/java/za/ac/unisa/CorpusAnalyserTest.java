package za.ac.unisa;

import org.junit.jupiter.api.Test;
import za.ac.unisa.CorpusAnalyser;

public class CorpusAnalyserTest {

  private static final String NEWS_PAPER_CORPUS = "/home/christopher/Desktop/unisa/news_paper_corpus";
  private static final String ARTICLE = "/home/christopher/Desktop/unisa/analysis_corpus.txt";
  private static final String SMALL_TEST = "/home/christopher/Desktop/unisa/small_test";
  private static final String SMALL_TEST2 = "/home/christopher/Desktop/unisa/small_test2";
  private static final String OUT = "/home/christopher/Desktop/unisa/DATA";

  @Test
  public void CorpusAnalyserTest() throws Exception {
    CorpusAnalyser.main(new String[] {ARTICLE,NEWS_PAPER_CORPUS, OUT});
  }

  @Test
  public void CorpusAnalyserTest2() throws Exception {
    CorpusAnalyser.main(new String[] {ARTICLE});

  }

  @Test
  public void CorpusAnalyserTest3() throws Exception {
    CorpusAnalyser.main(new String[] {SMALL_TEST,SMALL_TEST2,OUT});
  }
}
