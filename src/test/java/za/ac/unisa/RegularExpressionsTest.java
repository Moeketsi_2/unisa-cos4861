package za.ac.unisa;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.jupiter.api.Test;

class RegularExpressionsTest {


  private static final String SIMPLE_LINE = "This is a simple sentence, that Mc'DonalD.";
  private static final String ENDING_IN_B =
      "Bob and bigBb bought themselves a light bulb, but forgot to get the knobs for his radio (model 700B).";

  private static final String REPEATING =
      "bOB BOB,the The, those things we found,herbert herbert,JP,JP";

  private static final String REPEATING_1 = "Zbab bab abbbab bbbbabaaa ";
  private static final String REPEATING_2 = "1 Zbab bab abbbab bbbbabaaa\n";

  /**
   * 1)Definition  Word: Seperated by space, punctiona line breaks
   * 2) Set of all alpabetic strings
   * 3) All lower case strings ending with b.
   */
  @Test
  void question_1() {
    String regex = "[A-Za-z]+";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(SIMPLE_LINE);
    while (matcher.find()) {
      System.out.println("---" + matcher.group(0));
    }
  }

  @Test
  void question_2() {
    String regex = "[^A-Z][a-z]+b";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(ENDING_IN_B);
    while (matcher.find()) {
      System.out.println("---" + matcher.group(0));
    }
  }

  @Test
  void question_3() {
    String regex = "[A-Za-z]/\1";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(ENDING_IN_B);
    while (matcher.find()) {
      System.out.println("---" + matcher.group(0));
    }
  }

  @Test
  void question_4() {
    String regex = "[^A-Zc-z][a-b]*bab+[a-b]*[^A-Cc-z]";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(REPEATING_1);
    while (matcher.find()) {
      System.out.println("---" + matcher.group(0));
    }
  }

  @Test
  void question_5() {
    String regex = "^[0-9]+[A-Za-z0-9]?[A-Za-z]$";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(REPEATING_2);
    while (matcher.find()) {
      System.out.println("---" + matcher.group(0));
    }
  }
}