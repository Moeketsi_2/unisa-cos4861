package za.ac.unisa;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import za.ac.unisa.model.CountAndIndex;
import za.ac.unisa.model.CountIndexName;
import za.ac.unisa.model.SimplePair;
import za.ac.unisa.model.SimpleReport;

public class CorpusAnalyser {

  private static final String FAILED_OR_COULD_NOT_FIND_SPECIFIED_FILES = "FAILED OR COULD NOT FIND SPECIFIED FILES";

  public static void main(String args[]) {
    try {
      System.out.println(String.format("These are the arguments %s", Arrays.deepToString(args)));
      LPUtils lpUtils = new LPUtils();
      File corpus1 = new File(args[0]);
      File corpus2 = new File(args[1]);
      List<String> linesC1 = FileUtils.readLines(corpus1, StandardCharsets.UTF_8);
      Map<String, CountAndIndex> c1Unigrams = lpUtils.countUnigrams(linesC1);
      int[][] c1Bigrams = lpUtils.buildBigramMatrix(c1Unigrams);

      List<String> linesC2 = FileUtils.readLines(corpus2, StandardCharsets.UTF_8);
      Map<String, CountAndIndex> c2Unigrams = lpUtils.countUnigrams(linesC2);
      int[][] c2Bigrams = lpUtils.buildBigramMatrix(c2Unigrams);

      Map<String, SimplePair> unigramSimilarities = lpUtils.getSimilarities(c1Unigrams, c2Unigrams);
      List<Map> unigramDifferences = lpUtils.getDifferences(c1Unigrams, c2Unigrams);
      List<CountIndexName> topNUnigram1 = lpUtils.orderByCount(c1Unigrams, 100);
      List<CountIndexName> topNUnigram2 = lpUtils.orderByCount(c2Unigrams, 100);
      SimpleReport simpleReport = new SimpleReport();
      simpleReport.setBigramsCorpus1(c1Bigrams);
      simpleReport.setBigramsCorpus2(c2Bigrams);
      simpleReport.setTopNUnigram1(topNUnigram1);
      simpleReport.setTopNUnigram2(topNUnigram2);
      simpleReport.setUnigram1(c1Unigrams);
      simpleReport.setUnigram2(c2Unigrams);
      simpleReport.setUnigramDifferences(unigramDifferences);
      simpleReport.setUnigramSimilarities(unigramSimilarities);
      writeReport(simpleReport, args[2]);
      printMap(c1Unigrams);
      /* unigramsAndBigrams(c1Bigrams, c1Unigrams);*/
    } catch (Exception e) {
      throw new RuntimeException(FAILED_OR_COULD_NOT_FIND_SPECIFIED_FILES, e);
    }
  }

  private static void printMap(Map<String, CountAndIndex> stringCountAndIndexMap) {
    System.out.println(String.format("Unique Word Count %d", stringCountAndIndexMap.keySet().size()));
    StringBuilder line = new StringBuilder();
    int count = 0;
    for (String key : stringCountAndIndexMap.keySet()) {
      count++;
      if (count % 20 == 0) {
        System.out.println(line);
        line = new StringBuilder();
      } else {
        line.append(String.format("%s:%d\t", key, stringCountAndIndexMap.get(key).count.get()));
      }
    }
    System.out.println(line);
  }

  private static void writeReport(SimpleReport simpleReport, String parentPath) throws IOException {
    unigramsAndBigrams(simpleReport.getBigramsCorpus1(), simpleReport.getUnigram1(), parentPath, "corpus-1");
    unigramsAndBigrams(simpleReport.getBigramsCorpus2(), simpleReport.getUnigram2(), parentPath, "corpus-2");
    writeSimilarities(simpleReport.getUnigramSimilarities(), parentPath, "similarities");
  }

  private static void unigramsAndBigrams(int[][] bigram,
                                         Map<String, CountAndIndex> unigram,
                                         String parentFolder,
                                         String prefix) throws IOException {
    StringBuilder unigramsComplete = new StringBuilder();
    StringBuilder bigramsComplete = new StringBuilder();
    StringBuilder headerHeader = new StringBuilder();
    int count = 0;
    for (String column : unigram.keySet()) {
      headerHeader.append(column);
      if (count != unigram.size() - 1) {
        headerHeader.append("\t");
      }
      count++;
    }

    StringBuilder unigrams = new StringBuilder();
    count = 0;
    for (String column : unigram.keySet()) {
      if (count != unigram.size() - 1) {
        unigrams.append(unigram.get(column).count.get());
        unigrams.append("\t");
      } else {
        unigrams.append(unigram.get(column).count.get());
      }
      count++;
    }

    unigramsComplete.append(headerHeader);
    unigramsComplete.append("\n");
    unigramsComplete.append(unigrams);

    bigramsComplete.append(headerHeader);
    bigramsComplete.append("\n");
    int row = 0;
    for (String key : unigram.keySet()) {
      StringBuilder line = new StringBuilder(key);
      line.append("\t");
      int[] matrixRow = bigram[row];
      int column = 0;
      for (String otherWord : unigram.keySet()) {
        double prob = (double) matrixRow[column] / unigram.get(otherWord).count.get();
        if (column == unigram.size() - 1) {
          line.append(String.format("%.2f", prob));
        } else {
          line.append(String.format("%.2f\t", prob));
        }
        column++;
      }
      bigramsComplete.append(line);
      bigramsComplete.append("\n");
      row++;
    }
    File parent = new File(parentFolder);
    if (!parent.isDirectory()) {
      parent.mkdirs();
    }

    FileUtils.writeByteArrayToFile(new File(parent, String.format("%s-unigrams.tsv", prefix)),
        unigramsComplete.toString().getBytes(StandardCharsets.UTF_8));
    FileUtils.writeByteArrayToFile(new File(parent, String.format("%s-bigrams.tsv", prefix)),
        bigramsComplete.toString().getBytes(StandardCharsets.UTF_8));

  }

  private static void writeSimilarities(Map<String, SimplePair> unigramSimilarities, String parent, String prefix) {
    try {
      StringBuilder complete = new StringBuilder();
      StringBuilder headerHeader = new StringBuilder();
      int count = 0;
      for (String column : unigramSimilarities.keySet()) {
        headerHeader.append(column);
        if (count != unigramSimilarities.size() - 1) {
          headerHeader.append("\t");
        }
        count++;
      }
      complete.append(headerHeader);
      complete.append("\n");
      StringBuilder line = new StringBuilder();
      for (String word : unigramSimilarities.keySet()) {
        line.append(unigramSimilarities.get(word).first);
        if (count != unigramSimilarities.size() - 1) {
          line.append("\t");
        }
        count++;
      }
      line.append("\n");
      count = 0;
      for (String word : unigramSimilarities.keySet()) {
        line.append(unigramSimilarities.get(word).second);
        if (count != unigramSimilarities.size() - 1) {
          line.append("\t");
        }
        count++;
      }
      line.append("\n");
      complete.append(line);
      FileUtils.writeByteArrayToFile(new File(parent, String.format("%s-similarities.tsv", prefix)),
          complete.toString().getBytes(StandardCharsets.UTF_8));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
