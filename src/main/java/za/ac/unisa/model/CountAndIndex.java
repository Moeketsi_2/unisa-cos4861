package za.ac.unisa.model;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class CountAndIndex {
  public AtomicInteger count;
  public Set<Integer> indexKey;

  public CountAndIndex() {
    this.count = new AtomicInteger(1);
    this.indexKey = new HashSet<>();
  }

  public Integer increment() {
    return count.incrementAndGet();
  }

  public void addIndex(int index) {
    indexKey.add(index);
  }

  public int isBefore(CountAndIndex countAndIndex) {
    int count = 0;
    for (Integer index : countAndIndex.indexKey) {
      if (indexKey.contains(index - 1)) {
        count++;
      }
    }
    return count;
  }
}
