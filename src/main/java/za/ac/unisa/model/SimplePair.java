package za.ac.unisa.model;

public class SimplePair {
  public final int first;
  public final int second;

  public SimplePair(int first, int second) {
    this.first = first;
    this.second = second;
  }
}
