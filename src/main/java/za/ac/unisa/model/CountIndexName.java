package za.ac.unisa.model;

import java.util.Objects;

public class CountIndexName {
  public final String word;
  public final CountAndIndex countAndIndex;

  public CountIndexName(String word, CountAndIndex countAndIndex) {
    this.word = word;
    this.countAndIndex = countAndIndex;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CountIndexName that = (CountIndexName) o;
    return Objects.equals(word, that.word) && Objects.equals(countAndIndex, that.countAndIndex);
  }

  @Override
  public int hashCode() {
    return Objects.hash(word, countAndIndex);
  }
}
