package za.ac.unisa.model;

import java.util.List;
import java.util.Map;

public class SimpleReport {
  private Map<String, CountAndIndex> unigram1;
  private Map<String, CountAndIndex> unigram2;
  private Map<String, SimplePair> unigramSimilarities;
  private List<Map> unigramDifferences;
  private List<CountIndexName> topNUnigram1;
  private List<CountIndexName> topNUnigram2;
  private int[][] bigramsCorpus1;
  private int[][] bigramsCorpus2;

  public Map<String, CountAndIndex> getUnigram1() {
    return unigram1;
  }

  public void setUnigram1(Map<String, CountAndIndex> unigram1) {
    this.unigram1 = unigram1;
  }

  public Map<String, CountAndIndex> getUnigram2() {
    return unigram2;
  }

  public void setUnigram2(Map<String, CountAndIndex> unigram2) {
    this.unigram2 = unigram2;
  }

  public Map<String, SimplePair> getUnigramSimilarities() {
    return unigramSimilarities;
  }

  public void setUnigramSimilarities(Map<String, SimplePair> unigramSimilarities) {
    this.unigramSimilarities = unigramSimilarities;
  }

  public List<Map> getUnigramDifferences() {
    return unigramDifferences;
  }

  public void setUnigramDifferences(List<Map> unigramDifferences) {
    this.unigramDifferences = unigramDifferences;
  }

  public List<CountIndexName> getTopNUnigram1() {
    return topNUnigram1;
  }

  public void setTopNUnigram1(List<CountIndexName> topNUnigram1) {
    this.topNUnigram1 = topNUnigram1;
  }

  public List<CountIndexName> getTopNUnigram2() {
    return topNUnigram2;
  }

  public void setTopNUnigram2(List<CountIndexName> topNUnigram2) {
    this.topNUnigram2 = topNUnigram2;
  }

  public int[][] getBigramsCorpus1() {
    return bigramsCorpus1;
  }

  public void setBigramsCorpus1(int[][] bigramsCorpus1) {
    this.bigramsCorpus1 = bigramsCorpus1;
  }

  public int[][] getBigramsCorpus2() {
    return bigramsCorpus2;
  }

  public void setBigramsCorpus2(int[][] bigramsCorpus2) {
    this.bigramsCorpus2 = bigramsCorpus2;
  }
}
