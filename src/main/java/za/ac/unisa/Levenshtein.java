package za.ac.unisa;

import java.util.Scanner;


/**
 * Class calculates the za.ac.unisa.Levenshtein distance between two words.
 **/
public class Levenshtein {

  public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter Two words:");
    String word1 = scanner.nextLine();
    String word2 = scanner.nextLine();
    int[][] L = new int[word1.length()][word2.length()];
    for (int row = 0; row < word1.length(); row++) {
      for (int column = 0; column < word2.length(); column++) {
        L[row][0] = row;
        L[0][column] = column;
      }
    }
    for (int row = 1; row < word1.length(); row++) {
      for (int column = 1; column < word2.length(); column++) {
        L[row][column] = getMin(row, column, L, word1, word2);
      }
    }

    StringBuilder matrix = new StringBuilder();
    for (int row = 0; row < word1.length(); row++) {
      StringBuilder line = new StringBuilder();
      for (int column = 0; column < word2.length(); column++) {
        line.append(L[word1.length() - row - 1][column]);
        line.append(",");
      }
      matrix.append(line);
      matrix.append("\n");
    }
    System.out.println(matrix);
    System.out
        .println(String.format("The minimum number of change will be %d", L[word1.length() - 1][word2.length() - 1]));
  }


  private static int delete(int row, int column, int[][] L) {
    return L[row - 1][column] + 1;
  }

  private static int insert(int row, int column, int[][] L) {
    return L[row][column - 1] + 1;
  }

  private static int substitute(int row, int column, int[][] L, String word1, String word2) {
    if (word1.charAt(row - 1) == word2.charAt(column - 1)) {
      return L[row - 1][column - 1];
    } else {
      return L[row - 1][column - 1] + 2;
    }
  }


  private static int getMin(int row, int column, int[][] L, String word1, String word2) {
    int delete = delete(row, column, L);
    int insert = insert(row, column, L);
    int sub = substitute(row, column, L, word1, word2);
    int min = Math.min(Math.min(delete, insert), sub);
    /** System.out.println(String.format("\n\n\t\t\t |%d|\n D(%d,%d)=min\t |%d| =%d \n\t\t\t |%d|",
     delete,row,column,insert,min,sub));*/
    return min;
  }
}
