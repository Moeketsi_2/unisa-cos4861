package za.ac.unisa;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import za.ac.unisa.model.CountAndIndex;
import za.ac.unisa.model.CountIndexName;
import za.ac.unisa.model.SimplePair;

public class LPUtils {

  private static final String wordRegex = "[A-Za-z]+";
  Pattern pattern = Pattern.compile(wordRegex);

  public Map<String, CountAndIndex> countUnigrams(List<String> lines) {
    Map<String, CountAndIndex> stats = new TreeMap<>();
    int currentIndex = 0;
    for (String line : lines) {
      Matcher matcher = pattern.matcher(line);
      while (matcher.find()) {
        String currentMatch = matcher.group(0);
        System.out.println(currentMatch);
        if (!stats.containsKey(currentMatch)) {
          CountAndIndex countAndIndex = new CountAndIndex();
          stats.put(currentMatch, countAndIndex);
          countAndIndex.indexKey.add(currentIndex);
        } else {
          CountAndIndex countAndIndex = stats.get(currentMatch);
          countAndIndex.increment();
          countAndIndex.indexKey.add(currentIndex);
        }
        currentIndex++;
      }
    }
    return stats;
  }

  public int[][] buildBigramMatrix(Map<String, CountAndIndex> unigrams) {
    int[][] doubles = new int[unigrams.size()][unigrams.size()];
    int row = 0;
    for (String word : unigrams.keySet()) {
      int column = 0;
      for (String nextWord : unigrams.keySet()) {
        doubles[row][column] = unigrams
            .get(nextWord)
            .isBefore(unigrams.get(word));
        column++;
      }
      row++;
    }
    return doubles;
  }

  /**
   * Gets the similarities between two unigrams.
   */
  public Map getSimilarities(Map<String, CountAndIndex> unigrams,
                             Map<String, CountAndIndex> secondUnigrams) {
    Map similarities = new TreeMap();
    for (String word : unigrams.keySet()) {
      if (secondUnigrams.containsKey(word)) {
        similarities.put(word,
            new SimplePair(unigrams.get(word).count.get(),
                secondUnigrams.get(word).count.get()));
      }
    }
    return similarities;
  }

  public List<Map> getDifferences(Map<String, CountAndIndex> unigrams,
                                  Map<String, CountAndIndex> secondUnigrams) {
    Map onlyFoundInUnigram1 = new TreeMap<String, CountAndIndex>();
    Map onlyFoundInUnigram2 = new TreeMap<String, CountAndIndex>();
    for (String word : unigrams.keySet()) {
      if (!secondUnigrams.containsKey(word)) {
        onlyFoundInUnigram1.put(word, unigrams.get(word));
      }
    }

    for (String word : secondUnigrams.keySet()) {
      if (!unigrams.containsKey(word)) {
        onlyFoundInUnigram2.put(word, unigrams.get(word));
      }
    }
    return Arrays.asList(onlyFoundInUnigram1, onlyFoundInUnigram2);
  }

  public List<CountIndexName> orderByCount(Map<String, CountAndIndex> unigrams, int maxNumberOfElements) {
    List<CountIndexName> tempList = new LinkedList<>();
    for (String word : unigrams.keySet()) {
      tempList.add(new CountIndexName(word, unigrams.get(word)));
    }
    Collections.sort(tempList, new Comparator<CountIndexName>() {
      @Override
      public int compare(CountIndexName countIndexName, CountIndexName other) {
        var diff = countIndexName.countAndIndex.count.get() - other.countAndIndex.count.get();
        if (diff > 0) {
          return -1;
        } else if (diff < 0) {
          return 1;
        }
        return 0;
      }
    });
    return tempList;
  }
}
