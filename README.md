## Prerequisites before running the program.

1) You have to have installed java to run the program. Any java 1.8+ will do.
2) If you want to compile the code locally on your machine, you have to install mvn and a jdk with version greater then
    8.

## Compiling

`mvn clean package`
This should return an output similar to this:

```
WARNING: Illegal reflective access by com.google.inject.internal.cglib.core.$ReflectUtils$1 (file:/usr/share/maven/lib/guice.jar) to method java.lang.ClassLoader.defineClass(java.lang.String,byte[],int,int,java.security.ProtectionDomain)
WARNING: Please consider reporting this to the maintainers of com.google.inject.internal.cglib.core.$ReflectUtils$1
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release
[INFO] Scanning for projects...
[INFO] 
[INFO] -------------------------< za.ac.unisa:unisa >--------------------------
[INFO] Building unisa 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ unisa ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] Copying 0 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.6.1:compile (default-compile) @ unisa ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ unisa ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] Copying 3 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.6.1:testCompile (default-testCompile) @ unisa ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ unisa ---
[INFO] Surefire report directory: /home/christopher/workspace/private/unisa/target/surefire-reports

-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running za.ac.unisa.LevenshteinTest
Tests run: 0, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0 sec
Running za.ac.unisa.CorpusAnalyserTest
Tests run: 0, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0 sec

Results :

Tests run: 0, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ unisa ---
[INFO] 
[INFO] --- maven-assembly-plugin:3.1.1:single (make-assembly) @ unisa ---
[INFO] Building jar: /home/christopher/workspace/private/unisa/target/cos4861-jar-with-dependencies.jar
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.168 s
[INFO] Finished at: 2021-05-25T07:07:17+02:00
[INFO] ------------------------------------------------------------------------

```

## Running the code.

### Levenshtein Distance

`java -cp <path-to-jar>/cos4861-jar-with-dependencies.jar za.ac.unisa.Levenshtein`
in my case I was in the root source folder and had compiled the code so my command looked as follows:
`java -cp cos4861-jar-with-dependencies.jar za.ac.unisa.Levenshtein`
The terminal should prompt you for to enter two words.

```
christopher@christopher-ThinkPad-P15-Gen-1  ~/workspace/private/unisa  java -cp target/unisa-1.0-SNAPSHOT.jar za.ac.unisa.Levenshtein
Enter Two words:
```

Enter the two words. in the image I have entered

```
qweryer
aesrtye
```

Hit `Enter` or `Return` and an output will be displayed on the terminal.

```text
6,7,6,7,6,7,6,
5,6,5,6,5,6,5,
4,5,4,5,4,5,6,
3,4,3,4,5,6,7,
2,3,4,5,6,7,8,
1,2,3,4,5,6,7,
0,1,2,3,4,5,6,

The minimum number of change will be 6
```

This shows the minimum edit distance matrix,

```
6,7,6,7,6,7,6,
5,6,5,6,5,6,5,
4,5,4,5,4,5,6,
3,4,3,4,5,6,7,
2,3,4,5,6,7,8,
1,2,3,4,5,6,7,
0,1,2,3,4,5,6,
```

, and the minimum number of changes

`The minimum number of change will be 6`

### Corpus Analysis

To Run the corpus analysis run:

`java -cp <path-to-jar>/cos4861-jar-with-dependencies.jar za.ac.unisa.CorpusAnalyser <path-to-first-corpus> <path-to-second-corpus> <output-path>`

Example:

```
java -cp target/cos4861-jar-with-dependencies.jar za.ac.unisa.CorpusAnalyser src/test/resources/full_test/analysis_corpus.txt src/test/resources/full_test/news_paper_corpus src/test/resources/out
```
In the `<output-path>`, in the example shown as: `src/test/resources/out`, you will find 4 files:

```text
corpus-1-bigrams.tsv
corpus-1-unigrams.tsv
corpus-2-bigrams.tsv
corpus-2-unigrams.tsv
similarities-similarities.tsv
```
showing unigrams and bigrams for the two corpora. The file are prefixed with `corpus-<number>-unigram.tsv` 
and `corpus-<number>-bigram.tsv`, where the number corresponds to the order in which you entered the arguments in the 
program. For example `corpus-1` corresponds to `src/test/resources/full_test/analysis_corpus.txt` and `corpus-2` 
corresponds to `src/test/resources/full_test/news_paper_corpus`. `similarities-similarities.tsv` was going to be used
for further analysis.

The files can then be imported to `matlab` or `octave` for analysis. In my case I used `octave`. 





 